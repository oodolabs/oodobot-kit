"use strict";

const logs = require('logs');
const _ = require('lodash');
const assert = require('assert');
const util = require('util');
const url = require('url');
const config = require('../config');
const utils = require('../utils');
const resolver = require('../resolver');
const rpc = require('../rpc');
const Bus = require('../bus');
const ServiceClient = require('./service-client');
const Discovery = require('./discovery');

module.exports = Connection;

function Connection(clientId, options) {
  if (!(this instanceof Connection)) {
    return new Connection(clientId, options);
  }
  this.id = clientId;

  this.log = logs.get(clientId + '.connection');

  let settings = config.get('mqtt', {});

  // apply options
  if (options) settings = Object.assign(settings, options);

  let url = settings.url;
  if (!url) {
    settings.protocol = settings.protocol || 'mqtt';
    settings.host = settings.host || '127.0.0.1';
    settings.port = settings.port || '1883';
    url = util.format('%s://%s:%d', settings.protocol, settings.host, settings.port);
  }

  this.log.info("Connecting to %s using cid:%s", url, clientId);

  const bus = this.bus = Bus.connect(url, clientId);

  this.rpcServer = new rpc.Server(bus);

  this.services = [];

  const discovery = new Discovery(this);
  this.exportService(discovery, '/service/discover', '$discover');
}

Connection.prototype.ready = function (cb) {
  cb = cb || utils.createPromiseCallback();
  this.bus.ready(cb);
  return cb.promise;
};

Connection.prototype.close = function (done) {
  if (this.bus.connected) {
    this.bus.end(true, done);
  } else {
    done();
  }
};

/**
 *
 * @param topic incoming topic
 * @param raw message with raw payload as data
 * @param cb callback(err, message) message is {params: ..., slats: ..., data: ...}
 * @returns {*}
 */
Connection.prototype.subscribe = function (topic, raw, cb) {
  const that = this;

  if (typeof raw === 'function') {
    cb = raw;
    raw = undefined;
  }

  cb = cb || utils.noop();

  const sub = this.bus.subscribe(topic, function (topic, payload, message) {
    let data = payload;
    if (!raw) {
      if (!payload.params) {
        return that.log.warn("Invalid response for rpc call to", topic, payload);
      }
      data = payload.params;
    }

    if (!cb(data, message)) {
      // The callback has returned false, indicating that it does not want to receive any more messages,
      // so we can cancel the subscription.
      sub.cancel();
    }

  }, function (err) {
    if (err) throw err;
  });

  return sub;
};

Connection.prototype.publish = function (topic, payload, cb) {
  this.bus.publish(topic, payload, cb);
};

Connection.prototype.createServiceClient = function (topicOrAnnouncement) {
  return new ServiceClient(this, topicOrAnnouncement);
};

Connection.prototype.exportApp = function (app) {
  assert(app.info, '"info" is required for app');
  assert(app.info.id, '"id" is required for app.info');

  const that = this;

  const announcement = _.pick(app.info, 'id');
  if (!announcement.id) {
    throw new Error("You must provide an ID in the package.json");
  }
  const topic = util.format("$node/%s/app/%s", config.serial(), announcement.id);

  this.exportService(app, '/service/app', topic, announcement);

  if (config.get('autostart')) {
    this.createServiceClient(topic).request("start", [{}], 20000, function (err, error, result) {
      if (err || error) {
        that.log.error("Failed to auto-start driver:", err || error)
      }
    });
  }
};

Connection.prototype.exportDriver = function (driver) {
  setTimeout(this._exportDriver.bind(this, driver), 3000);
};

Connection.prototype._exportDriver = function (driver) {
  assert(driver.info, '"info" is required for driver');
  assert(driver.info.id, '"id" is required for driver.info');

  const that = this;

  const announcement = _.pick(driver.info, 'id');
  const topic = util.format("$node/%s/driver/%s", config.serial(), announcement.id);
  this.exportService(driver, '/service/driver', topic, announcement);

  if (config.get("autostart")) { // auto start driver
    this.createServiceClient(topic).request("start", [{}], 20000, function (err, error, result) {
      if (err || error) {
        that.log.error("Failed to auto-start driver:", err || error)
      }
    });
  }
};

Connection.prototype.exportDevice = function (device, announcement) {
  announcement = _.cloneDeep(announcement || device.announcement);

  assert(announcement && typeof announcement === 'object', 'Invalid announcement type: ' + typeof announcement);
  assert(announcement.naturalIdType, 'announcement.naturalIdType is required');
  assert(announcement.naturalId, 'announcement.naturalId is required');

  announcement.id = announcement.id || utils.guid(announcement.naturalIdType, announcement.naturalId);
  const topic = "$device/" + announcement.id;
  this.exportService(device, '/service/device', topic, announcement);
};

Connection.prototype.exportChannel = function (device, channel, protocol, id, supportedMethods, supportedEvents) {
  if (!protocol) {
    return new Error("The channel must have a protocol. Channel ID: " + id)
  }

  const topic = util.format("$device/%s/channel/%s", device.id, id);

  const announcement = {
    id: id,
    protocol: protocol,
    supportedMethods: supportedMethods,
    supportedEvents: supportedEvents
  };

  this.exportService(channel, resolver.resolveProtocolURI(protocol), topic, announcement)
};

Connection.prototype.exportService = function (service, serviceUri, topic, announcement, mapping) {
  serviceUri = resolver.resolveSchemaURI(serviceUri);

  announcement = _.clone(announcement || {});
  announcement.nid = config.serial();
  announcement.topic = topic;
  announcement.schema = serviceUri;

  const exportedService = this.rpcServer.registerService(service, topic, serviceUri, mapping);

  if (!announcement.supportedMethods) {
    announcement.supportedMethods = exportedService.methods;
  } else if (announcement.supportedMethods.length > exportedService.methods.length) {
    throw new Error("The number of actual exported methods is less than the number said to be exported. " +
      "Check the method signatures of the service. topic: " + announcement.topic)
  }

  announcement.supportedEvents = announcement.supportedEvents || [];

  exportedService.sendEvent('announce', announcement);

  this.log.debug("Exported service on topic: %s (schema: %s) with methods: %j",
    announcement.topic, announcement.schema, announcement.supportedMethods);

  service.sendEvent = function (event, payload) {
    return exportedService.sendEvent(event, payload);
  };

  this.services.push(announcement);

  return exportedService;
};

Connection.prototype.notify =
Connection.prototype.sendNotification = function (topic, payload, cb) {
  return this.rpcServer.sendNotification(topic, payload, cb);
}
