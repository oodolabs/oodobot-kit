"use strict";

const _ = require('lodash');
const minimatch = require('minimatch');
const resolver = require('../resolver');
const errors = require('../errors');

module.exports = Discovery;

/**
 * Discover Service
 *
 * @constructor
 */
function Discovery(conn) {
  this.conn = conn;
}

Discovery.prototype.services = function (schema, callback) {
  if (!schema || schema === '') {
    return callback(null, this.conn.services);
  }

  try {
    schema = resolver.resolveSchemaURI(schema);
    const matching = _.filter(this.conn.services, function (service) {
      return minimatch(service.schema, schema, { matchBase: true });
    });
    callback(null, matching);
  } catch (e) {
    callback(errors.toJsonRPCError(e));
  }

};
