"use strict";

const path = require('path');

module.exports = Mod;

function Mod() {

}

Object.defineProperty(Mod.prototype, 'info', {
  get: function () {
    if (this._info !== undefined) {
      return this._info;
    }
    if (this._pkginfo === undefined) {
      try {
        this._pkginfo = require(path.resolve('./package.json'));
      } catch (e) {
        this._pkginfo = null;
      }
    }
    return this._pkginfo || this._info;
  },
  set: function (info) {
    this._info = info;
  }
});

Mod.prototype.sendEvent = function (event, payload) {
  throw new Error('Illegal State: module has not been exported');
};
