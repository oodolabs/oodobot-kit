"use strict";

const util = require('util');

exports.connect = exports.connection = exports.Connection = require('./connection');

exports.Mod = require('./mod');
exports.mod = function (Klass) {
  util.inherits(Klass, exports.Mod);
};

exports.App = require('./app');
exports.app = function (Klass) {
  util.inherits(Klass, exports.App);
};

exports.Driver = require('./driver');
exports.driver = function (Klass) {
  util.inherits(Klass, exports.Driver);
};

exports.Device = require('./device');
exports.device = function (Klass) {
  util.inherits(Klass, exports.Device);
};
