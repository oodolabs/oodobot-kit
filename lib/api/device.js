"use strict";

const _ = require('lodash');
const util = require('util');
const Mod = require('./mod');
const utils = require('../utils');
const res = require('../resources');
const resolver = require('../resolver');

module.exports = Device;

function Device(driverOrConn, options) {
  if (!(this instanceof Device)) {
    return new Device(driverOrConn, options);
  }
  if (driverOrConn) {
    if (driverOrConn.conn) {
      this.conn = driverOrConn.conn;
      this.parentLog = driverOrConn.log;
    } else if (driverOrConn.exportService) {
      this.conn = driverOrConn;
    } else {
      options = driverOrConn;
      driverOrConn = null;
    }
  }

  options = options || {};
  this.conn = this.conn || options.conn;

  let parentLog = this.parentLog || options.log || '';
  if (typeof parentLog === 'string') {
    parentLog = require('../logger').get(parentLog);
  }
  this.parentLog = parentLog;

  _.assign(this, _.omit(options, ['conn', 'log']));
}

util.inherits(Device, Mod);

Object.defineProperty(Device.prototype, 'log', {
  get: function () {
    if (!this._log) {
      this.assert();
      this._log = this.parentLog.extend('[device] ' + this.idType + ':' + this.id);
    }
    return this._log;
  }
});

Device.prototype.assert = function () {
  if (!this.conn || typeof this.id === 'undefined' || !this.idType) {
    throw new Error('You must provide values for "conn", "id" and "idType" properties in your device.');
  }
};

Device.prototype.export = function () {
  this.assert();

  const topic = res.topics.device.service.device(this.guid);
  this.conn.exportService(this, '/service/device', topic.publish, this.announcement);
};

Device.prototype.exportChannel = function (/*protocol, id, channel, supportedEvents*/) {
  this.assert();

  let protocol, id, channel, supportedEvents;

  _.forEach(arguments, function (arg) {
    if (!protocol && _.isString(arg)) {
      protocol = arg;
    } else if (!id && _.isString(arg)) {
      id = arg;
    } else if (!supportedEvents && Array.isArray(arg)) {
      supportedEvents = arg;
    } else if (!channel && _.isObject(arg)) {
      channel = arg;
    }
  });

  if (!protocol) {
    throw new Error('You must provide at least a protocol for this channel.');
  }

  id = id || protocol;
  channel = channel || this;

  const topic = res.topics.device.channel.service.device(this.guid).channel(id);

  const announcement = {
    id: id,
    protocol: protocol
  };

  if (supportedEvents) {
    announcement.supportedEvents = supportedEvents;
  }

  this.conn.exportService(channel, resolver.resolveProtocolURI(protocol), topic.publish, announcement);
};

Object.defineProperty(Device.prototype, 'guid', {
  get: function () {
    return utils.guid(this.idType, this.id);
  }
});

Object.defineProperty(Device.prototype, 'announcement', {
  get: function () {
    if (typeof this.id === 'undefined' || !this.idType) {
      throw new Error('You must provide values for "id" and "idType" properties in your device.');
    }
    return {
      id: this.guid,
      naturalId: this.id,
      naturalIdType: this.idType,
      name: this.name,
      signatures: this.signatures || {}
    }
  }
});

//Object.defineProperty(Device.prototype, 'log', {
//  get: function () {
//    throw new Error('You must set "driver", "id" and "idType" before you can use the log');
//  }
//});
//
//definePrecedentProperties('driver', 'id', 'idType');
//
//function definePrecedentProperties() {
//  _.forEach(arguments, function (name) {
//    Object.defineProperty(Device.prototype, name, {
//      set: function (value) {
//        utils.defineReadonlyProperty(this, name, value, true);
//        tryInitiateLog(this);
//      }
//    });
//  });
//}
//
//function tryInitiateLog(device) {
//  try {
//    const log = device.log;
//  } catch (e) {
//    if (device.driver && device.id && device.idType) {
//      if (!device.driver.log) {
//        throw new Error('The "driver" you add to a device object must have a "log" property.');
//      }
//
//      if (!device.driver.conn) {
//        throw new Error('The "conn" you add to a device object must have a "conn" property.');
//      }
//
//      utils.defineReadonlyProperty(device, 'log', device.driver.log.extend('[device] ' + device.idType + ':' + device.id));
//      utils.defineReadonlyProperty(device, 'conn', device.conn);
//    }
//  }
//}
