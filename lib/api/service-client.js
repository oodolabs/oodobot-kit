"use strict";

const rpc = require('../rpc');

module.exports = ServiceClient;

function ServiceClient(conn, options) {
  if (typeof options === 'string') {
    options = {topic: options}
  }

  this.conn = conn;
  this.topic = options.topic;

  if (options.supportedMethods) {
    this.supportedMethods = options.supportedMethods;
  }

  if (options.supportedEvents) {
    this.supportedEvents = options.supportedEvents;
  }

  this.rpc = new rpc.Client(conn.bus, this.topic);
}

ServiceClient.prototype.onEvent = function (event, callback) {
  return this.conn.subscribe(this.topic + "/event/" + event, callback);
};

ServiceClient.prototype.request = function (method, params, timeout, callback) {
  if (typeof timeout === 'function') {
    callback = timeout;
    timeout = 0;
  }

  return this.rpc.request(method, params, {timeout: timeout}, callback);
};
