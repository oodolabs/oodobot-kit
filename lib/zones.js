"use strict";

const fs = require('fs');

const zoneFile = '/usr/share/zoneinfo/zone.tab';
const tzFile = '/etc/timezone';

exports.getLocation = function getLocation() {
  if (fs.existsSync(tzFile) && fs.existsSync(zoneFile)) {
    const zoneData = fs.readFileSync(zoneFile, {encoding: 'utf8'});
    const timezone = fs.readFileSync(tzFile, {encoding: 'utf8'}).replace(/\n/, '');

    const lookupArray = zoneData.split(/[\n\t]/);
    const index = lookupArray.indexOf(timezone);

    if (index === -1) {
      return;
    }

    const latLongStr = lookupArray[index - 1];
    const lat = parseInt(latLongStr);
    const lon = parseInt(latLongStr.replace((lat > 0) ? '+' + lat : lat, ''));

    return {
      latitude: lat / 100,
      longitude: lon / 100
    }
  }
};

