"use strict";

const url = require('url');

const rootSchemaURL = 'http://schema.oodo.co';
const protocolSchemaURL = 'http://schema.oodo.co/protocol/';

exports.resolveSchemaURI = resolveSchemaURI;
function resolveSchemaURI(uri) {
  return resolveSchemaURIWithBase(rootSchemaURL, uri)
}

exports.resolveProtocolURI = resolveProtocolURI;
function resolveProtocolURI(uri) {
  return resolveSchemaURIWithBase(protocolSchemaURL, uri)
}

exports.resolveProtocolURI = resolveProtocolURI;
function resolveSchemaURIWithBase(base, uri) {
  return url.resolve(base, uri);
}

