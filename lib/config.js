const util = require('util');
const EventEmitter = require('events').EventEmitter;
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const Provider = require('nconf').Provider;
const configup = require('configup');
const chalk = require('chalk');
const {execSync} = require('child_process');

const logger = require('./logger');

function Config() {
  if (!(this instanceof Config)) {
    return new Config();
  }

  EventEmitter.call(this);

  this.nconf = new Provider();

  this.settings = {};

  this.log = logger.get('config');
}

util.inherits(Config, EventEmitter);

Config.prototype.isPaired =
  Config.prototype.isActivated = function () {
    return this.get('token') && this.get('owner');
  };

Config.prototype.get = function (name, defaultValue) {
  if (name === undefined || name === null) {
    return this.nconf.get(name);
  }

  let i, subname, value;

  name = name.toString();
  i = name.indexOf('.');
  if (i > 0) {
    subname = name.substring(i + 1);
    name = name.substring(0, i);
  }
  value = this.nconf.get(name);
  if (value && subname) {
    value = _.get(value, subname);
  }
  return value === undefined ? defaultValue : value;
};

Config.prototype.duration = function (name, defaultValue) {
  const val = this.get(name, defaultValue);
  return require('parse-duration')(val);
};

Config.prototype.set = function (name, value) {
  return this.settings[name] = value;
};

Config.prototype.getOodoHome = function () {
  let home = this.get('OODO_HOME');
  if (!home) {
    home = path.resolve(process.cwd(), '..');
    const basename = path.basename(home);
    if (basename === 'drivers' || basename === 'apps' || basename === 'node_modules' || basename === 'modules') {
      home = path.resolve(home, '..');
    }
  }
  return home;
};

Config.prototype.getUserHome = function () {
  return process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
};

Config.prototype.getDataPath = function () {
  return this.get('DATA_HOME') || '/data';
};

Config.prototype.resolveVariablePath = function (variable, relatives) {
  const args = [this.get(variable)].concat(relatives);
  return path.resolve.apply(path, args);
};

Config.prototype.resolvePath = function (base, relatives) {
  return path.resolve.apply(path, _.flattenDeep(arguments));
};

Config.prototype.resolveOodoHome = function () {
  return this.resolvePath(this.getOodoHome(), arguments);
};

Config.prototype.resolveUserHome = function () {
  return this.resolvePath(this.getUserHome(), arguments);
};

Config.prototype.resolveDataHome = function () {
  return this.resolvePath(this.getDataPath(), arguments);
};

Config.prototype.add = function (settings) {
  this.nconf.add('conf-' + this._nameId++ , {type: 'literal', store: settings});
};

Config.prototype.load = function (path, name, env) {
  name = name || 'config';
  env = env || this.env;
  const settings = configup.load(path, name, {env});
  if (this.log.isDebugEnabled()) {
    print_load_message(util.format('Load from %s', path), !settings);
  }
  return settings;
};

Config.prototype.file = function (path, name, env) {
  const settings = this.load(path, name, env);
  if (settings) this.add(settings);
};

Config.prototype.refresh = function (overrides) {
  this.nconf = new Provider();

  this._nameId = 0;

  if (overrides) {
    this._overrides = overrides;
  }

  // parse argv
  const argv = require('yargs').argv;

  // get env
  let env = argv.env || process.env.NODE_ENV || 'development';

  let settings = _.omit(argv, ['_', '$0']);

  if (typeof this._overrides === 'object') {
    settings = _.defaults(settings, _.cloneDeep(this._overrides));
    env = settings.env || env;
  }

  // config logger using argv for env log
  this.configLogger(settings);

  if (this.log.isInfoEnabled()) {
    console.log(chalk.gray('-------------------------------------------------------'));
    console.log(chalk.gray(':: ' + require('dateformat')(new Date(), 'yyyy-mm-dd HH:MM:ss.l')));
    console.log();
    console.log(chalk.blue.bold('Environment : ' + env + ' '));
    console.log(chalk.gray('-------------------------------------------------------'));
  }

  if (typeof this._overrides === 'string') {
    settings = this.load(this._overrides, env);
    this.configLogger(settings);
  }

  this.settings = settings = settings || {};
  // set env to process.env and settings.env
  this.env = settings.env = env;

  // local literal store
  this.add(this.settings);

  // config logger using argv for env log
  this.configLogger();

  // add env
  if (this.log.isDebugEnabled()) {
    print_load_message('Load environment variables');
  }
  this.nconf.env('__');

  // common credentials config
  this.file(this.resolveOodoHome('config'), 'credentials');

  // common environment(s) config
  this.file(this.resolveOodoHome('config'), 'config');

  // development config
  this.file(this.resolveOodoHome('oodobot-config'), 'config');

  // home directory environment(s) config
  this.file(this.resolveUserHome('.oodobot'), 'config');

  // mesh file
  this.file(this.resolveDataHome('etc/oodobot'), 'mesh');

  // credentials file
  this.file(this.resolveDataHome('etc/oodobot'), 'credentials');

  // Add site preference overrides
  this.file(this.resolveDataHome('etc/oodobot'), 'site-preferences');

  // User overrides (json)
  this.file(this.resolveDataHome('etc/oodobot'), 'config');

  // current directory environment(s) config
  this.file(path.resolve('.', 'config'), 'config');

  this.configLogger();

  this.log.debug('Config loaded successful');

  this.emit('refresh');
};

Config.prototype.configLogger = function (options) {
  if (typeof options === 'string') {
    options = {log: options};
  }
  options = options || {};

  let level = options.log || this.get('log');
  if (!level) {
    if (this.get('trace') || this.get('verbose') || options.trace || options.verbose) {
      level = 'trace';
    } else if (this.get('debug') || options.debug) {
      level = 'debug';
    } else {
      level = 'info'
    }
  }

  logger.setLevel(level);
};

// ---------------------------------------------
// Custom config
// ---------------------------------------------
Config.prototype.serial = function () {
  const sn = this.get('serial');
  if (sn) return sn;

  try {
    let result = execSync('oodobot-serial') || '';
    result = result.toString().trim();
    return this.set('serial', result);
  } catch (e) {
    const msg = 'Failed to get device serial (oodobot-serial must be in the PATH) error: ' + e.message;
    this.log.error(msg);
    throw new Error(msg);
  }
};

Config.prototype.oodobotVersion = function () {
  const version = this.get('oodobot-version');
  if (version) return version;

  try {
    const result = execSync('oodobot-version') || '';
    return result.toString().trim();
  } catch (e) {
    const msg = 'Failed to get device serial (oodobot-version must be in the PATH) error: ' + e.message;
    this.log.error(msg);
    throw new Error(msg);
  }
};

function print_load_message(message, fail) {
  const label = fail ? chalk.gray('×') : chalk.green('√');
  message = fail ? chalk.gray(message) : chalk.green(message);
  console.log('  ', label, message);
}

exports = module.exports = new Config();
exports.Config = Config;
