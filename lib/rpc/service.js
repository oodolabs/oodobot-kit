"use strict";

const log = require('logs').get('service');
const util = require('util');
const P = require('bluebird');
const schemas = require('../schemas');

class Service {
  constructor(topic, methods, schema, server) {
    this.topic = topic;
    this.methods = methods;
    this.schema = schema;
    this.server = server;
  }

  sendEvent(event, payload) {
    const that = this;
    const uri = this.schema + '#/events/' + event + '/value';

    log.debug("Validating event '%s' (schema: %s). Payload: %j", event, uri, payload);

    const hasPayload = payload !== undefined && payload !== null;

    if (event !== 'announce') {
      const schema = schemas.getSchema(uri);

      if (hasPayload && !schema) {
        return P.reject(new Error(util.format("Event '%s' failed validation (schema: %s). A payload wasn't defined, but one was given.", event, uri)));
      }

      if (!hasPayload && schema) {
        return P.reject(new Error(util.format("Event '%s' failed validation (schema: %s). A payload was defined, but none was given.", event, uri)))
      }

      const result = schemas.validate(schema, payload);
      if (result) {
        return P.reject(new Error(util.format("Failed to validate event %s on service %s. Error:%j", event, that.schema, result)));
      }
    }

    return that.server.sendNotification(that.topic + "/event/" + event, payload);
  }
}

module.exports = Service;
