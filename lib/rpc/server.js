"use strict";

const _ = require('lodash');
const remjson = require('remjson');
const Service = require('./service');
const schemas = require('../schemas');
const utils = require('../utils');

function Server(bus, log) {
  if (!(this instanceof Server)) {
    return new Server(bus, log);
  }
  this.bus = bus;
  this.log = log || require('logs').get('kit:rpc.server');
  this._servers = {};
}

Server.prototype.notify =
Server.prototype.sendNotification = function (topic, payload, cb) {
  cb = cb || utils.createPromiseCallback();

  const raw = {
    jsonrpc: '2.0',
    params: payload,
    time: Math.floor(Date.now() / 1000)
  };

  if (!_.endsWith(topic, "/module/status") && this.log.isDebugEnabled()) {
    this.log.debug("< Outgoing to %s : %j", topic, raw);
  }

  this.bus.publish(topic, raw, cb);

  return cb.promise;
};

Server.prototype.registerService = function (receiver, topic, schema, mapping) {
  const methods = schemas.getServiceMethods(schema);
  if (!methods) {
    return;
  }

  const exportedMethods = _.filter(_.keys(methods), function (name) {
    return _.isFunction(receiver[name]);
  });

  const server = this._getServer(topic, true);

  mapping = mapping || {};
  _.forEach(exportedMethods, function (m) {
    server.method(mapping[m] || m, receiver[m], receiver);
  });

  server.transport = server.mqtt(this.bus, topic, this.log);

  return new Service(topic, exportedMethods, schema, this);
};

Server.prototype._getServer = function (topic, createIfNotExists) {
  if (!this._servers[topic] && createIfNotExists) {
    this._servers[topic] = remjson.server();
  }
  return this._servers[topic];
};

module.exports = Server;
