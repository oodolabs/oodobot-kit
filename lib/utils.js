'use strict';

const util = require('util');
const crypto = require('crypto');
const _ = require('lodash');
const pkginfo = require('pkginfo');

const Topicer = require('./topicer');

let moduleName = process.cwd().substring(process.cwd().lastIndexOf('/') + 1);

if (moduleName === 'driver-combined') {
  moduleName = 'drivers';
}

exports.moduleName = moduleName;

function nop() {
  // no-op
}

exports.nop = exports.noop = nop;

exports.createPromiseCallback = createPromiseCallback;

function createPromiseCallback() {
  const PromiseA = require('bluebird');
  let cb = nop;
  const promise = new PromiseA(function (resolve, reject) {
    cb = function (err, data) {
      if (err) return reject(err);
      return resolve(data);
    };
  });
  cb.promise = promise;

  cb.resolve = function (result) {
    cb(undefined, result);
    return promise;
  };

  cb.reject = function (err) {
    cb(err);
    return promise;
  };

  return cb;
}

function toString(obj) {
  if (obj === undefined || obj === null) {
    return '';
  }
  return obj.toString();
}

exports.addPackageJson = function addPackageJson(owner, dir) {
  owner = owner || {};
  try {
    const info = {exports: {}};
    pkginfo(info, {
      dir: dir,
      include: ['topics', 'services']
    });
    owner.services = _.assign({}, info.exports.services, owner.services);
    owner.topics = _.assign({}, Topicer.apply(info.exports.topics), owner.topics);
  } catch (e) {
    //console.log('XXX - Failed to find package.json in directory', dir);
  }
  return owner;
};

exports.defineReadonlyProperty = function (object, name, value, enumerable) {
  const options = {writable: false, enumerable: enumerable, configurable: false};
  if (typeof value === 'function') {
    options.get = value;
  } else {
    options.value = value;
  }
  Object.defineProperty(object, name, options);
};


/**
 * Just removes any characters that don't match [a-zA-Z0-9-_.]
 *
 * @method
 * @param {String} input The string to be made safe
 * @returns {String} The identifier which is safe to use as an identifier (e.g for devices)
 */
exports.safeId = function (input) {
  if (typeof input !== 'string') {
    return '-BAD_IDENTIFIER-';
  }
  return (input + '').replace(/[^a-zA-Z0-9-_\.]/g, '');
};

exports.guid = function (idType, id) {
  return exports.hash(idType + '.' + exports.safeId(id));
};

exports.hash = function (value) {
  const data = _.map(arguments, toString).join('');
  return crypto.createHash('sha256').update(data).digest('hex').substring(0, 10);
};
