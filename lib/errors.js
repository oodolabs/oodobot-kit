"use strict";

const errors = require('errors');

const codes = exports.codes = {
  OPERATION_ERROR: 1001
};

exports.toJsonRPCError = function (code, message) {
  if (typeof code !== 'number') {
    message = code;
    code = null;
  }
  if (message && typeof message === 'object') {
    message = message.message;
    code = code || message.code;
  }

  code = code || codes.OPERATION_ERROR;
  message = message || 'Unknown Error';
  return {
    code: code,
    message: message
  }
};

exports.TimeoutError = errors.create({name: 'TimeoutError'});
exports.InvalidArgumentError = errors.create({name: 'InvalidArgumentError'});
