"use strict";

const duration = /(-?\d*\.?\d+(?:e[-+]?\d+)?)\s*([a-zμ]*)/ig

module.exports = interval;

/**
 * conversion ratios
 */

interval.nanosecond =
  interval.ns = 1 / 1e6;

interval.μs =
  interval.microsecond = 1 / 1e3;

interval.millisecond =
  interval.ms = 1;

interval.second =
  interval.sec =
    interval.s = interval.ms * 1000;

interval.minute =
  interval.min =
    interval.m = interval.s * 60;

interval.hour =
  interval.hr =
    interval.h = interval.m * 60;

interval.day =
  interval.d = interval.h * 24;

interval.week =
  interval.wk =
    interval.w = interval.d * 7;

interval.month = interval.d * (365.25 / 12);

interval.year =
  interval.yr =
    interval.y = interval.d * 365.25;

/**
 * convert `str` to ms
 *
 * @param {String|Number} str
 * @return {Number}
 */

function interval(str) {
  if (typeof str === 'number') {
    return str;
  }
  let result = 0;
  // ignore commas
  str = str.replace(/(\d),(\d)/g, '$1$2');
  str.replace(duration, function (_, n, units) {
    units = interval[units]
      || interval[units.toLowerCase().replace(/s$/, '')]
      || 1;
    result += parseFloat(n) * units
  });
  return result
}
