"use strict";

const path = require('path');
const utils = require('./utils');

utils.addPackageJson(exports, path.resolve(__dirname, '..')); // From kit
utils.addPackageJson(exports, process.cwd()); // From a driver/module/etc.
