"use strict";

const debug = require('debug')('kit:schemas');
const _ = require('lodash');
const path = require('path');
const url = require('url');
const config =require('./config');

const rootSchemaUrl = 'http://schema.oodo.co/';

const schemas = loadSchemas();
const tools = schemas.tools;

exports.getSchema = function (schema, baseUri) {
  let name;
  if (typeof schema === 'string') {
    const parts = schema.split('#');
    schema = parts[0];
    if (parts.length > 1) {
      name = parts[1].split('/').filter(s => s).join('.');
    }
  }

  const result = tools.getSchema(schema, baseUri);
  return name ? _.get(result, name) : result;
};

/**
 *
 * @param schemaUri
 * @param data
 * @param failMissing
 * @returns {null|string|error} null for success. string or error for error.
 */
exports.validate = function (schemaUri, data, failMissing) {
  return tools.validate(schemaUri, data, failMissing);
};

exports.resolveUrl = function (root, uri) {
  if (!uri) {
    uri = root;
    root = rootSchemaUrl;
  }
  return url.resolve(root, uri);
};

exports.getServiceMethods = function (service) {
  return exports.getSchema(service + '#/methods');
};

/////////////////////////////////////////////////////////////
function findLibrary(dirs) {
  dirs = Array.isArray(dirs) ? dirs : [dirs];
  debug('try to find module in');
  _.forEach(dirs, function (dir) {
    debug('  -', dir);
  });
  const found = _.find(dirs, function (dir) {
    try {
      return require(dir);
    } catch (e) {
      if (e && !/Cannot find module/.test(e.message)) {
        throw e;
      }
    }
  });
  if (found) {
    debug('found module', found);
    return require(found);
  } else {
    debug('Can not find module in', dirs);
  }
}

function loadSchemas(possibles) {
  possibles = possibles || [];
  possibles = Array.isArray(possibles) ? possibles : [possibles];
  possibles = possibles.concat([
    config.resolveOodoHome('schemas'),
    config.resolveOodoHome('oodo-schemas'),
    'oodo-schemas'
  ]);

  //log.debug('Try to load schemas from', possibles);

  const schemas = findLibrary(possibles);
  if (!schemas) throw new Error('Can not find oodo-schemas');
  return schemas;
}
