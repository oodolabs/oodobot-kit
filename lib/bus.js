"use strict";

const _ = require('lodash');
const util = require('util');
const mqttr = require('mqttr');
const config = require('./config');
const utils = require('./utils');

exports.connect = function (url, options, parentLog) {
  if (typeof options === 'string') {
    options = {clientId: options};
  }

  let log;
  if (typeof parentLog === 'string') {
    log = require('logs').get(parentLog).extend('MQTT Bus', 'yellow');
  } else if (parentLog) {
    log = parentLog.extend('MQTT Bus', 'yellow');
  } else {
    log = require('logs').get('MQTT Bus', 'yellow');
  }

  const id = (options && options.clientId) || 'unknown-' + Date.now();

  const topic = util.format("$node/%s/module/%s/state/connected", config.serial(), id);

  options = _.assign({
    clientId: id,
    codec: config.get('codec', 'json'),
    will: {
      topic: topic,
      payload: false,
      qos: 0,
      retain: true
    }
  }, options);


  console.log('Connecting to', url);

  const client = mqttr.connect(url, options, log);
  client.ready(function () {
    client.publish(topic, true, {retain: true});
  });

  return client;
};

exports.get = function (log) {
  if (typeof log === 'string') {
    log = require('logs').get(log);
  } else if (!log) {
    log = require('logs').get('bus');
  }

  const options = config.get('mqtt.options');

  options.clientId = utils.moduleName;

  log.debug('Connecting to MQTT server %s', config.get('mqtt.url'), options);

  const bus = exports.connect(config.get('mqtt.url'), options, log);

  bus.on('connect', function () {
    log.debug('Connected');
  });

  bus.on('close', function () {
    log.warn('Disconnected');
  });

  bus.setMaxListeners(666);

  return bus;
};
