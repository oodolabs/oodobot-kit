"use strict";

const _ = require('lodash');
const Houkou = require('houkou');
const util = require('util');

function Topicer(topic, params, config) {
  if (!(this instanceof Topicer)) {
    return new Topicer(topic, params, config);
  }

  this.params = params || {};
  this.config = config || {};

  this.topic(topic);
}

Object.defineProperty(Topicer.prototype, 'subscribe', {
  get: function () {
    return this.route.build(this.params);
  }
});

Object.defineProperty(Topicer.prototype, 'publish', {
  get: function () {
    for (const param in this.params) {
      if (this.params[param] == '+') {
        throw new Error('You have not set a value for the "' + param + '" parameter. Cannot publish to a wildcard.');
      }
    }
    return this.route.build(this.params);
  }
});

Topicer.prototype.match = function (topic) {
  const match = this.match_route.match(topic);

  for (const param in match) {
    if (this.params[param] != '+' && (this.params[param] + '') !== match[param]) {
      return null;
    }
  }
  return match;
};

Topicer.prototype.topic = function (topic) {
  this.baseTopic = topic;

  this.route = new Houkou(topic.replace(/\$/, "\\$"));
  this.match_route = new Houkou(topic.replace(/\$/, "\\$"), requirements(topic));

  const that = this;
  _.forEach(this.route.parameters, function (name) {
    if (typeof that.params[name] === 'undefined') {
      that.params[name] = '+';
    }
    that[name] = function (val) {
      if (typeof val === 'undefined' || val === null) {
        throw new Error('Null or undefined value provided for parameter "' + name + '"');
      }
      if (val.indexOf('/') > -1) {
        throw new Error('Value "' + val + '" for parameter "' + name + '" contains a forward-slash.');
      }
      this.params[name] = val;
      return this;
    };
  });
};

Topicer.prototype.withReply = function () {
  return new Topicer(this.baseTopic + '/reply', this.params, this.config);
};

Topicer.prototype.getTopic = function () {
  return this.baseTopic;
};

Topicer.prototype.setParams = function (vals) {
  _.assign(this.params, vals);
  return this;
};

Topicer.prototype.qos = function (qos) {
  this.config.qos = qos;
  return this;
};

Topicer.prototype.retain = function (retain) {
  this.config.retain = retain;
  return this;
};

Topicer.prototype.timeout = function (timeout) {
  this.config.timeout = timeout;
  return this;
};

Topicer.prototype.toString = function () {
  return util.format('[Topicer topic=%s params=%s config=%s]',
    this.baseTopic.green, util.inspect(this.params, {colors: true}), util.inspect(this.config, {colors: true}));
};

Topicer.apply = function (obj) {
  _.forEach(obj, function (val, key) {
    if (typeof val === 'string') {
      obj.__defineGetter__(key, function () {
        return new Topicer(val);
      });
    } else if (typeof val === 'object') {
      Topicer.apply(val);
    }
  });
  return obj;
};

// Stolen from mqtt-router
function requirements(route) {

  const params = route.match(/:[a-zA-Z0-9]+/g);

  if (!params) {
    return null;
  }

  const obj = {requirements: {}};

  params.forEach(function (param) {
    obj.requirements[param.replace(':', '')] = "[.a-zA-Z0-9_-]+";
  });

  return obj;
}

module.exports = Topicer;
