"use strict";

const _ = require('lodash');
const winston = require('winston');
const logs = require('logs').use('winston');

const chalk = require('chalk');

// log4js.configure({
//   appenders: [
//     {
//       type: 'console',
//       layout: {
//         type: 'pattern',
//         pattern: chalk.gray('[%d{ISO8601}]') + ' %[%-5p%] | ' + chalk.blue.bold('%c') + ' - %m'
//       }
//     }
//   ]
// });

exports.setLevel = function (level) {
  // console.log('Set logger level to', level);
  setLevel(winston, level);

  function setLevel(logger, level) {
    logger.level = level;
    if (logger.loggers && logger.loggers.loggers) {
      _.forEach(logger.loggers.loggers, function (l) {
        setLevel(l, level);
      });
    }
  }
};

exports.get = function (name, color, category, colors) {
  name = name || '[NONAME]';

  colors = colors ? colors.slice() : ['cyan', 'magenta', 'green', 'grey'];
  category = category ? category.slice() : [];

  if (color) {
    colors.unshift(color);
  }

  category.push(chalk[colors.shift() || 'white'](name));

  const l = logs.get(category.join(' '));
  l.level = winston.level;

  /**
   * Extends the logger with an additional category
   *
   * @method extend
   * @param {String} name The added category name
   * @param {String} [color] The color of the category name when in logs (using module "colors")
   */
  l.extend = function (name, color) {
    return exports.get(name, color, category, colors);
  };

  return l;
};
