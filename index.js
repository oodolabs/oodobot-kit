'use strict';

const util = require('util');
const crypto = require('crypto');
const pkginfo = require('pkginfo');
const chalk = require('chalk');

const utils = require('./lib/utils');
const zones = require('./lib/zones');
const logger = require('./lib/logger');
const resources = require('./lib/resources');

const moduleName = utils.moduleName;

const kit = {};

kit.errors = require('./lib/errors');
kit.config = require('./lib/config');
kit.schemas = require('./lib/schemas');
kit.bus = kit.Bus = require('./lib/bus');
kit.rpc = require('./lib/rpc');
kit.api = require('./lib/api');
kit.utils = require('./lib/utils');
kit.interval = require('./lib/interval');

kit.topics = resources.topics;
kit.services = resources.services;

kit.init = function (settings) {
  kit.config.refresh(settings);
  return this;
};

kit.getLog = (function () {
  const log = logger.get(moduleName);

  return function (category) {
    return log.extend(category);
  };
})();

// TODO move below to boot phase styles ?

// location
kit.location = zones.getLocation();

// print configs
kit.config.on('refresh', function () {
  const log = kit.getLog('config');
  if (kit.config.get('dconfig') && log.isInfoEnabled()) {
    console.log(chalk.gray('================= CONFIGURATION BEGIN ================='));
    console.log(util.inspect(this.get(), {colors: true}));
    console.log(chalk.gray('================== CONFIGURATION END =================='));
  }

  // log memory
  if (kit.config.get('logmemory') || kit.config.get('logMemory')) {
    // initMemWatch()
    console.warn('memory log is disabled temporary');
  }

// bugsnag
  const bugsnagKey = kit.config.get('bugsnagKey') || kit.config.get('bugsnag');
  if (bugsnagKey) {
    initBugSnag(bugsnagKey);
  }
});

module.exports = kit;

// function initMemWatch() {
//   const log = kit.getLog('[MEMORY]', 'red');
//   const memwatch = exports.memwatch = require('@airbnb/node-memwatch');
//   let hd;
//
//   memwatch.on('leak', function (info) {
//     log.info('Leak', info);
//   });
//
//   memwatch.on('stats', function (stats) {
//     log.info('Stats', stats);
//     if (hd) log.info('Diff', util.inspect(hd.end(), {colors: true, depth: null}));
//     hd = new memwatch.HeapDiff();
//   });
// }

function initBugSnag(bugsnagKey) {
  const bugsnag = require("bugsnag");
  const os = require('os');

  const getPackageJson = function (dir) {
    const packageJson = {exports: {}};
    pkginfo(packageJson, {
      dir: dir
    });
    return packageJson;
  };

  const cleanConfig = JSON.parse(JSON.stringify(kit.config.get()));
  cleanConfig.token = cleanConfig.token ? '[EXISTS]' : null;

  bugsnag.register(bugsnagKey, {
    autoNotify: false,
    userId: cleanConfig.userId,
    logger: kit.getLog('BugSnag')
  });

  process.on('uncaughtException', function (e) {
    bugsnag.metaData = {
      severity: 'error',
      "User": {
        serial: kit.config.serial()
      },
      "Host": {
        platform: process.platform,
        arch: process.arch,
        hostname: os.hostname(),
        uptime: os.uptime(),
        freemem: os.freemem(),
        totalmem: os.totalmem(),
        cpus: os.cpus()
      },
      "Node.JS": process.versions,
      "Memory Usage": process.memoryUsage(),
      "Config": cleanConfig,
      "Application": {
        module: getPackageJson(process.cwd()),
        common: getPackageJson(__dirname)
      }
    };

    return bugsnag.notify(e);
  });

  kit.getLog('BugSnag').info('Starting...');
}


