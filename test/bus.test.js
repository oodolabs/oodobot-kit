'use strict';

const t = require('chai').assert;
const s = require('./support');
const kit = require('../');

describe('Bus', function () {
  let server;

  before(() => s.buildMqttServer().then(s => server = s));

  after(function (done) {
    server.close(done);
  });

  describe('test', () => {
    let bus;

    beforeEach(function (done) {
      bus = kit.bus.connect(server.url);
      bus.ready(done);
    });

    afterEach(function (done) {
      bus.end(done);
      bus = null
    });

    it('should work', function (done) {
      bus.subscribe('$hello/:name', function (topic, message, matched) {
        t.equal(matched.params.name, 'foo');
        t.deepEqual(message, {a: 1});
        done();
      });

      bus.publish('$hello/foo', {a: 1});

    });

    it('should stop handle after cancel', function (done) {
      let i = 0;
      const sub = bus.subscribe('$hello/:name', function () {
        if (i === 0) return ++i;
        t.fail('Should not run here');
      });

      bus.publish('$hello/foo', {a: 1});
      sub.cancel();
      bus.publish('$hello/foo', {a: 1});

      setTimeout(done, 500);

    });
  });

});
