"use strict";

module.exports = OnOff;

function OnOff() {
  this._state = false;
}

OnOff.prototype.set = function (state, cb) {
  cb(null, this._state = state);
};

OnOff.prototype.turnOn = function (cb) {
  cb(null, this._state = true);
};

OnOff.prototype.turnOff = function (cb) {
  cb(null, this._state = false);
};

OnOff.prototype.toggle = function (cb) {
  cb(null, this._state = !this._state);
};
