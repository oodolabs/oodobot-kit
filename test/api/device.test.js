"use strict";

const t = require('chai').assert;
const _ = require('lodash');
const randomstring = require('randomstring');
const s = require('../support');
const logger = require('../../lib/logger');
const api = require('../../').api;

describe('Device', function () {

  let server;

  before(() => s.buildMqttServer().then(s => server = s));

  after(function (done) {
    server && server.close(done);
  });

  describe('test', () => {
    let conn;

    beforeEach(function () {
      conn = api.Connection(randomstring.generate(10), {url: server.url});
    });

    afterEach(function (done) {
      conn.close(done);
    });

    it('should initiate if driver have no "log" property', function () {
      const device = new api.Device({conn: conn}, {idType: 'foo', id: '1'});
      device.assert();
      device.log.info('Should print this');
    });

    it('should initiate with custom properties', function () {
      const device = new api.Device({conn: conn}, {idType: 'foo', id: '1', conn: 'fake conn', foo: 'bar'});
      t.equal(device.foo, 'bar');
      t.equal(device.conn, conn);
    });

    it('should throw error if driver have no "conn" property', function () {
      t.throw(function () {
        const device = new api.Device({log: logger.get('test')}, {idType: 'foo', id: '1'});
        device.assert();
      });
    });

    it('should export device', function () {
      const device = new api.Device({conn: conn, log: logger.get('test')}, {idType: 'foo', id: '1'});
      device.export();
      t.ok(_.find(conn.services, {id: device.guid}));
    });

    it('should export channel', function () {
      const device = new api.Device({conn: conn, log: logger.get('test')}, {idType: 'foo', id: '1'});
      device.exportChannel('on-off');
      t.ok(_.find(conn.services, {id: 'on-off'}));
    });

    it('should export channel with custom id', function () {
      const device = new api.Device({conn: conn, log: logger.get('test')}, {idType: 'foo', id: '1'});
      device.exportChannel('on-off', 'abcd');
      t.ok(_.find(conn.services, {id: 'abcd'}));
    });

    it('should export channel with custom supportedEvents', function () {
      const device = new api.Device({conn: conn, log: logger.get('test')}, {idType: 'foo', id: '1'});
      device.exportChannel('on-off', ['state', 'data']);
      console.log(conn.services);
      const service = _.find(conn.services, {id: 'on-off'});
      t.ok(service);
      t.deepEqual(service.supportedEvents, ['state', 'data']);
    });
  });
});
