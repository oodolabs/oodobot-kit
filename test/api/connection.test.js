"use strict";

const t = require('chai').assert;
const s = require('../support');
const randomstring = require('randomstring');
const api = require('../../').api;

describe('Connection', function () {

  let server;

  before(() => s.buildMqttServer().then(s => server = s));

  after(function (done) {
    server && server.close(done);
  });

  it('should initiate', function (done) {
    const conn = new api.Connection(randomstring.generate(10), {url: server.url});
    conn.ready(function () {
      t.lengthOf(conn.services, 1);
      conn.close(done);
    });
  });

  it('should service client work', function (done) {
    const conn = new api.Connection(randomstring.generate(10), {url: server.url});
    conn.ready(function () {
      const service = conn.createServiceClient('$discover');
      service.request('services', ['**'], 1000, function (err, error, result) {
        t.notOk(err);
        t.notOk(error);
        t.deepEqual(result, conn.services);
        conn.close(done);
      });
    });
  });
});
