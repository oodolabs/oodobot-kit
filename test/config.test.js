"use strict";

const t = require('chai').assert;
const config = require('../').config;

const serialRegex = /^[a-z0-9]{6,254}$/i;

describe('config', function () {

  it('should initialize', function () {
    t.ok(config.get('env'));
  });

  it('should get serial', function () {
    const serial = config.serial();
    t.ok(serial);
    t.ok(serialRegex.test(serial));
  });

  it('should load settings in fixtures', function () {
    t.equal(config.get('foo'), null, 'development')
  });
});
