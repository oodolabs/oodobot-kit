"use strict";

const t = require('chai').assert;
const utils = require('../').utils;

describe('utils', function () {

  it('should generate different guid', function () {
    const id1 = utils.guid('node', 'OSXC02M2KEJFD57');
    const id2 = utils.guid('node', '00000000C27BBBD6');
    console.log(id1, id2);
    t.notEqual(id1, id2);
  });
});
