"use strict";

const t = require('chai').assert;
const schemas = require('../').schemas;

describe('schemas', function () {

  it('should resolve url', function () {
    t.equal(schemas.resolveUrl('protocol/on-off'), 'http://schema.oodo.co/protocol/on-off');
  });

  it('should get full schema', function () {
    //const schemas = new Schemas();
    //schemas.init(schemasRoot);
    const schema = schemas.getSchema('/protocol/on-off');
    t.ok(schema);
    t.equal(schema['$schema'], '/protocol');
  });

  it('should get cached schema', function () {
    //const schemas = new Schemas();
    //schemas.init(schemasRoot);
    const schema1 = schemas.getSchema('/protocol/on-off');
    const schema2 = schemas.getSchema('protocol/on-off');
    const schema3 = schemas.getSchema('http://schema.oodo.co/protocol/on-off');
    t.equal(schema1, schema2);
    t.equal(schema1, schema3);
  });

  it('should get a part of schema', function () {
    //const schemas = new Schemas();
    //schemas.init(schemasRoot);
    let part = schemas.getSchema('/protocol/on-off#/methods');
    t.ok(part);
    t.ok(part.set);

    part = schemas.getSchema('/protocol/on-off#/events/state/value');
    t.ok(part);
  });

  it('should get service methods', function () {
    //const schemas = new Schemas();
    //schemas.init(schemasRoot);
    const methods = schemas.getServiceMethods('/protocol/on-off');
    t.ok(methods);
    t.ok(methods.set);
  });
});
