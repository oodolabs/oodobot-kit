"use strict";

require('logs').use('winston');

const PromiseA = require('bluebird');
const mosca = require('mosca');

PromiseA.config({
  // Enable warnings
  warnings: true,
  // Enable long stack traces
  longStackTraces: true,
  // Enable cancellation
  cancellation: true,
  // Enable monitoring
  monitoring: true
});

let next_port = 43210;

exports.buildMqttServer = function buildMqttServer(options) {
  if (typeof options === 'number') {
    options = {port: options};
  }

  options = options || {};

  if (!options.port) options.port = next_port++;

  console.log('build mqtt server', options);
  return PromiseA.fromCallback(cb => new mosca.Server(options, cb)).then(server => {
    server.url = 'mqtt://localhost:' + options.port;
    server.port = options.port;
    return server;
  });
};

