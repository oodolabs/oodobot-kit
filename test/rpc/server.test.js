"use strict";

const t = require('chai').assert;
const s = require('../support');
const kit = require('../../');
const Server = kit.rpc.Server;

const demo_service = {
  bar: 'hello',
  foo: function () {

  },
  set: function () {

  }
};

describe('rpc/server', function () {

  let server, bus;

  before(() => s.buildMqttServer().then(s => server = s));

  after(function (done) {
    server.close(done);
  });

  beforeEach(function (done) {
    bus = kit.bus.connect(server.url);
    bus.ready(done);
  });

  afterEach(function (done) {
    bus.end(done);
  });

  it('should send notification', function (done) {
    const server = new Server(bus);
    const test_topic = '/test/event/hello';
    bus.subscribe('/test/event/hello', function (topic, payload) {
      t.equal(topic, test_topic);
      t.ok(payload);
      t.deepEqual(payload.params, {foo: 'bar'});
      done();
    });
    server.sendNotification(test_topic, {foo: 'bar'});
  });

  it('should register service', function () {
    const server = new Server(bus);
    const exportedService = server.registerService(demo_service, '$device/1234/channel/on-off', '/protocol/on-off');
    t.ok(exportedService);
    t.lengthOf(exportedService.methods, 1);
  });

  it('should send event', function (done) {
    const server = new Server(bus);
    const exportedService = server.registerService(demo_service, '$device/1234/channel/on-off', '/protocol/on-off');
    bus.subscribe('$device/1234/channel/on-off/event/state', function (topic, payload) {
      t.ok(payload);
      t.deepEqual(payload.params, true);
      done();
    });
    exportedService.sendEvent('state', true);
  });
});
