"use strict";

const t = require('chai').assert;
const s = require('../support');
const kit = require('../../');
const Server = kit.rpc.Server;
const Client = kit.rpc.Client;

const OnOff = require('../fixtures/channels/on-off');

describe('rpc/integration', function () {
  let server, bus, onoff;

  before(() => s.buildMqttServer().then(s => server = s));

  after(function (done) {
    server.close(done);
  });

  beforeEach(function (done) {
    onoff = new OnOff();
    bus = kit.bus.connect(server.url);
    bus.ready(done);
  });

  afterEach(function (done) {
    bus.end(done);
  });

  it('should work', function (done) {
    const topic = '$device/1234/channel/on-off';

    const server = new Server(bus);
    const client = new Client(bus, topic);

    server.registerService(onoff, '$device/1234/channel/on-off', '/protocol/on-off');
    client.request('set', {state: true}, function (err, error, result) {
      if (err) throw err;
      if (error) throw error;
      t.equal(result, true);
      done();
    });
  });

});
