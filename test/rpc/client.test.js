"use strict";

const t = require('chai').assert;
const s = require('../support');
const kit = require('../../');
const Client = kit.rpc.Client;

describe('rpc/client', function () {
  let server, bus;

  before(() => s.buildMqttServer().then(s => server = s));

  after(function (done) {
    server.close(done);
  });

  beforeEach(function (done) {
    bus = kit.bus.connect(server.url);
    bus.ready(done);
  });

  afterEach(function (done) {
    bus.end(done);
  });

  it('should request without timeout', function (done) {
    const client = new Client(bus, '/test');

    bus.subscribe('/test', function (topic, payload) {
      t.ok(payload);
      t.isObject(payload);
      t.equal(payload.method, 'foo');
      t.deepEqual(payload.params, {bar: 'hello'});
      done();
    });
    client.request('foo', {bar: 'hello'}, null, function () {
      // no-op
    });

  });

  it('should respond with in timeout', function (done) {
    const client = new Client(bus, '/test');
    bus.subscribe('/test', function (topic, payload) {
      function respond() {
        bus.publish('/test/reply', {
          jsonrpc: '2.0',
          id: payload.id,
          result: 'ok'
        });
      }

      setTimeout(respond, 100);
    });

    client.request('foo', {bar: 'hello'}, {timeout: 200}, function (err, error, result) {
      t.notOk(err);
      t.notOk(error);
      t.equal(result, 'ok');
      done();
    });
  });

  it('should throw timeout error', function (done) {
    const client = new Client(bus, '/test');
    bus.subscribe('/test', function (topic, payload) {
      function respond() {
        bus.publish('/test/reply', {
          jsonrpc: '2.0',
          id: payload.id,
          result: 'ok'
        });
      }

      setTimeout(respond, 200);
    });

    client.request('foo', {bar: 'hello'}, {timeout: 100}, function (err, error, result) {
      t.ok(err);
      t.equal(err.name, 'TimeoutError');
      t.notOk(error);
      t.notOk(result);
      done();
    });
  });
});
