"use strict";

const t = require('chai').assert;
const pkginfo = require('pkginfo');
const Topicer = require('../lib/topicer');

const topics = {
  "app": {
    "service": "$node/:node/app/:app",
    "message": "$node/:node/app/:app/message",
    "event": "$node/:node/app/:app/event/:event"
  }
};

describe('Topicer', function () {

  it('should create topic', function () {
    const topicer = new Topicer(topics.app.message);
    t.equal(topicer.baseTopic, topics.app.message);
    t.deepEqual(topicer.params, {node: '+', app: '+'});
    t.isFunction(topicer.node);
    t.isFunction(topicer.app);
  });

  it('should build publish topic', function () {
    const topicer = new Topicer(topics.app.message);
    topicer.node('123').app('456');
    t.equal(topicer.publish, '$node/123/app/456/message')
  });

  it('should apply all topics', function () {
    const topicers = Topicer.apply(topics);
    t.ok(topicers.app.service);
  });
});


