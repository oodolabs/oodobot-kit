"use strict";

process.title = 'oodobot-kit-test';

var kit = require('..');
kit.init({
  log: 'debug',
  logMemory: true
});

console.log('Serial:', kit.config.serial());

var arr = [];

setInterval(function () {
  arr.push(new Buffer(1024 * 1024));
}, 1000);

setInterval(function () {
  kit.memwatch.gc();
}, 5000);
